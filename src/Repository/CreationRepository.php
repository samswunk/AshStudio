<?php

namespace App\Repository;
use App\Entity\Creation;
use App\Entity\CreationRecherche;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Creation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Creation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Creation[]    findAll()
 * @method Creation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Creation::class);
    }

    public function findSearchQuery(CreationRecherche $search): Query
    {
        $query = $this->RetourneTousLesValides();

        if ($search->getTypeMatieres()->count()>0)
        {
            foreach($search->getTypeMatieres() as $k =>$typeMatiere) {
                $query = $query->andWhere(' :typematiere MEMBER OF b.typeMatieres')
                    ->setParameter('typematiere', $typeMatiere);
            }
        }/**/
        if ($search->getPrixMax())
        {
            $query  ->andWhere('b.prixCreation <= :prixmax')
                ->setParameter('prixmax',$search->getPrixMax());
        }
        if ($search->getPrixMin())
        {
            $query  ->andWhere('b.prixCreation >= :prixmin')
                ->setParameter('prixmin',$search->getPrixMin());
        }

        return $query->getQuery();
    }

    private function RetourneTousLesValides()
    {
        return $this->createQueryBuilder('b');
            //->andWhere('b.vendu = false');
    }
    /**/
    // /**
    //  * @return Creation[] Returns an array of Creation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Creation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

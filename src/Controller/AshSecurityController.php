<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AshSecurityController extends AbstractController
{
    /**
     * @Route("/login",name="login")
     */
    public function login (AuthenticationUtils $authenticationUtils)
    {
        $error          = $authenticationUtils->getLastAuthenticationError();
        $lastUserName   = $authenticationUtils->getLastUsername();

        return $this->render('security/index.html.twig',[
            'lastUserName'  =>$lastUserName,
            'error'         =>$error
        ]);
    }

}
?>
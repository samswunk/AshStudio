<?php

namespace App\Controller;

use App\Entity\Creation;
use App\Entity\CreationRecherche;
use App\Form\CreationRechercheType;
use App\Form\CreationType;
use App\Repository\CreationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

class AshStudioController extends AbstractController
{


    public function __construct(CreationRepository $repo, EntityManagerInterface $manager)
    {
        $this->repo = $repo;
        $this->manager = $manager;
    }
    /**
     * @Route("/ashstudio", name="ash_studio")
     * @param CreationRepository $repo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        //$listeCreations = $this->repo->findSearchQuery();
        $search = new CreationRecherche();
        $form = $this->createForm(CreationRechercheType::class, $search);
        $form->handleRequest($request);
        $listeCreations = $paginator->paginate(
            $this->repo->findSearchQuery($search), /* query NOT result */
            $request->query->getInt('page',1), /*page number*/
            8 /*limit per page*/
        );

        return $this->render('Creations/index.html.twig', [
            'controller_name'   =>'AshStudioController',
            'creations'         =>$listeCreations,
            'form'              =>$form->createView(),
            'rubrique'          =>'INDEX'
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        $creations = $this->repo->findAll();
        return $this->render('index.html.twig', [
            'creations'         =>$creations,
            'rubrique'          =>'HOME'
        ]);
    }
    /**
     * @Route("/edition/{id}", name="creation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,$id)
    {
        $creation = $this->repo->find($id);
        $form = $this->createForm(CreationType::class,$creation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $this->manager->persist($task);
            $this->manager->flush();
            $this->addFlash('success','Bien édité avec succès');
            return $this->redirectToRoute('creation_admin');
        }
        return $this->render('Creations/admin/edit.html.twig', [
            'form'         =>$form->createView(),
            'rubrique'     =>'EDIT'
        ]);
    }

    /**
     * @Route("/creation/new", name="creation_new", methods={"GET","POST"})
     */
    public function create(Request $request)
    {
        $creation = new Creation();
        $form = $this->createForm(CreationType::class,$creation);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $this->manager->persist($task);
            $this->manager->flush();
            $this->addFlash('success','Bien créé avec succès');
            return $this->redirectToRoute('creation_admin');
        }

        return $this->render('Creations/admin/new.html.twig', [
            'form'      => $form->createView(),
            'rubrique'  =>'NEW'
        ]);
    }

    /**
     * @Route("/creation/{id}", name="creation_show")
     */
    public function show($id)
    {
        $creation = $this->repo->find($id);
        return $this->render('Creations/show.html.twig', [
            'creations'         =>$creation,
            'rubrique'          =>'SHOW'
        ]);
    }

    /**
     * @Route("/admin/creations",name="creation_admin")
     * @return Response
     */
    public function admin()
    {
        $creations = $this->repo->findAll();
        return $this->render('Creations/admin/admin.html.twig', [
            'creations'         =>$creations,
            'rubrique'          =>'ADMIN'
        ]);
        /*return $this->render('property/admin/admin.html.twig',compact('biens'));
        return $this->render('property/admin/admin.html.twig',[
            'menu_courant'=>'home',
            'biens' => $biens
        ]);/**/
    }

    /**
     * @Route("/admin/delete/{id}",name="creation_delete", methods={"DELETE"})
     * @return Response
     */
    public function delete(Request $request, $id)
    {
        $creation = $this->repo->find($id);
        var_dump($creation);
        if ($this->isCsrfTokenValid('delete', $request->request->get('_token')))
        {
            $this->manager->remove($creation);
            $this->manager->flush();
            $this->addFlash('success','Création supprimée avec succès');/**/
            //return new Response('suppression de n° '.$id);
            return $this->redirectToRoute('creation_admin');
        }
        else
        {
            return new Response('token invalide : '.$request->get('_token'));
        }

    }
}

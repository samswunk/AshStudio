<?php

namespace App\Controller;

use App\Entity\Matiere;
use App\Form\MatiereType;
use App\Repository\MatiereRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

class MatiereController extends AbstractController
{
    public function __construct(MatiereRepository $repo, EntityManagerInterface $manager)
    {
        $this->repo = $repo;
        $this->manager = $manager;
    }
    /**
     * @Route("/matiere", name="matiere_index")
     * @param MatiereRepository $repo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $listeMatieres = $this->repo->findAll();
        return $this->render('matiere/index.html.twig', [
            'controller_name'   =>'AshStudioController',
            'matieres'          =>$listeMatieres,
            'rubrique'          =>'INDEX'
        ]);
    }

    /**
     * @Route("/matiere", name="matiere_home")
     */
    public function home()
    {
        $matieres = $this->repo->findAll();
        return $this->render('index.html.twig', [
            'matieres'         =>$matieres,
            'rubrique'          =>'HOME'
        ]);
    }
    /**
     * @Route("/matiere/edition/{id}", name="matiere_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,$id)
    {
        $matiere = $this->repo->find($id);
        $form = $this->createForm(MatiereType::class,$matiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $this->manager->persist($task);
            $this->manager->flush();
            $this->addFlash('success','Matière éditée avec succès');
            return $this->redirectToRoute('matiere_admin');
        }
        return $this->render('matiere/admin/edit.html.twig', [
            'form'         =>$form->createView(),
            'rubrique'     =>'EDIT'
        ]);
    }

    /**
     * @Route("/matiere/new", name="matiere_new", methods={"GET","POST"})
     */
    public function create(Request $request)
    {
        $matiere = new Matiere();
        $form = $this->createForm(MatiereType::class,$matiere);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $this->manager->persist($task);
            $this->manager->flush();
            $this->addFlash('success','Matière créée avec succès');
            return $this->redirectToRoute('matiere_admin');
        }

        return $this->render('matiere/admin/new.html.twig', [
            'form'      => $form->createView(),
            'rubrique'  =>'NEW'
        ]);
    }

    /**
     * @Route("/matiere/{id}", name="matiere_show")
     */
    public function show($id)
    {
        $matiere = $this->repo->find($id);
        return $this->render('matiere/show.html.twig', [
            'matieres'         =>$matiere,
            'rubrique'          =>'SHOW'
        ]);
    }

    /**
     * @Route("/admin/matieres",name="matiere_admin")
     * @return Response
     */
    public function admin()
    {
        $matieres = $this->repo->findAll();
        return $this->render('matiere/admin/admin.html.twig', [
            'matieres'         =>$matieres,
            'rubrique'          =>'ADMIN'
        ]);
        /*return $this->render('property/admin/admin.html.twig',compact('biens'));
        return $this->render('property/admin/admin.html.twig',[
            'menu_courant'=>'home',
            'biens' => $biens
        ]);/**/
    }

    /**
     * @Route("/admin/delete/{id}",name="matiere_delete", methods={"DELETE"})
     * @return Response
     */
    public function delete(Request $request, $id)
    {
        $matiere = $this->repo->find($id);
        var_dump($matiere);
        if ($this->isCsrfTokenValid('delete', $request->request->get('_token')))
        {
            $this->manager->remove($matiere);
            $this->manager->flush();
            $this->addFlash('success','Création supprimée avec succès');/**/
            //return new Response('suppression de n° '.$id);
            return $this->redirectToRoute('matiere_admin');
        }
        else
        {
            return new Response('token invalide : '.$request->get('_token'));
        }

    }
}

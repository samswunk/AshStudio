<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200103154104 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE creation (id INT AUTO_INCREMENT NOT NULL, type_matiere_id INT DEFAULT NULL, nom_creation VARCHAR(80) NOT NULL, description_creation VARCHAR(255) DEFAULT NULL, prix_creation INT DEFAULT NULL, image_creation VARCHAR(255) DEFAULT NULL, date_creation DATETIME DEFAULT NULL, INDEX IDX_57EE8574E96F047D (type_matiere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matiere (id INT AUTO_INCREMENT NOT NULL, nom_matiere VARCHAR(60) NOT NULL, description_matiere LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, nom_role VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, type_role_id INT DEFAULT NULL, nom_user VARCHAR(50) DEFAULT NULL, prenom_user VARCHAR(60) DEFAULT NULL, ddn_user DATETIME DEFAULT NULL, username VARCHAR(30) NOT NULL, email_user VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, INDEX IDX_8D93D649FE224D59 (type_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE creation ADD CONSTRAINT FK_57EE8574E96F047D FOREIGN KEY (type_matiere_id) REFERENCES matiere (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649FE224D59 FOREIGN KEY (type_role_id) REFERENCES roles (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE creation DROP FOREIGN KEY FK_57EE8574E96F047D');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649FE224D59');
        $this->addSql('DROP TABLE creation');
        $this->addSql('DROP TABLE matiere');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE user');
    }
}

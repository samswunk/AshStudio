<?php

namespace App\Form;

use App\Entity\Creation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\Matiere;

class CreationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomCreation')
            ->add('descriptionCreation')
            ->add('prixCreation')
            ->add('typeMatiere',EntityType::class, [
                'class'         => Matiere::class,
                'choice_label'  => 'nomMatiere',
                'multiple'      => false
            ])
            ->add('imageCreation')
            //->add('dateCreation')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Creation::class,
        ]);
    }
}

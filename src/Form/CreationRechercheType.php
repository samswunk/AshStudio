<?php

namespace App\Form;

use App\Entity\Matiere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\CreationRecherche;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
class CreationRechercheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prixMax',IntegerType::class, [
                'required'  => false,
                'attr'      => [
                    'placeholder'   =>'Prix maximum'
                ]
            ])
            ->add('prixMin',IntegerType::class, [
                'required'  => false,
                'attr'      => [
                    'placeholder'=>'Prix minimum'
                ]
            ])
            ->add('nomCreation',TextType::class, [
                'required'  => false,
                'attr'      => [
                    'placeholder'=>'Nom'
                ]
            ])
           /*->add('submit',SubmitType::class, [
                        'label' => 'Rechercher'
                        ])/**/
            /*->add('typeMatieres',EntityType::class, [
                'class'         => Matiere::class,
                'choice_label'  => 'nomMatiere',
                'label'         => false,
                'multiple'      => false
            ])/**/
            //->add('imageCreation')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'        =>CreationRecherche::class,
            'method'            =>'get',
            'csrf_protection'   =>false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}

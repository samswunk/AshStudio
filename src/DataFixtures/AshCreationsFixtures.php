<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Provider\DateTime;
use Faker\Provider\Lorem;
use Faker\Provider\Text;
use Faker\Provider\Company;
use Faker\Factory;
use App\Entity\Creation;

class AshCreationsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1;$i <= 22; $i++)
        {
            $faker = Factory::create('fr_FR');
            $creation = new Creation();
            $creation   ->setNomCreation($faker->sentence(1))
                ->setDescriptionCreation($faker->paragraph(2))
                ->setPrixCreation($faker->randomNumber(2))
                ->setImageCreation($faker->imageUrl($width = 200, $height = 100,'fashion'))
                ->setDateCreation(new \DateTime());
            //$faker->dateTime($max = 'now', $timezone = null)
            $manager->persist($creation);
        }
        $manager->flush();/**/
    }
}

<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AshUserFixture extends Fixture
{
    public function  __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder=$encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setNomUser('DEMOW');
        $user->setPrenomUser('John');
        $user->setDdnUser(new \DateTime());
        $user->setPassword($this->encoder->encodePassword($user, 'demo'));
        $user->setUsername('demo');
        $user->setEmailUser('demo@gmaimail.com');
        $manager->persist($user);
        $manager->flush();

        $user = new User();
        $user->setNomUser('BOULARD');
        $user->setPrenomUser('Edouard');
        $user->setDdnUser(new \DateTime());
        $user->setPassword($this->encoder->encodePassword($user, 'edboul'));
        $user->setUsername('Edboul');
        $user->setEmailUser('Edboul@gmaimail.com');
        $manager->persist($user);
        $manager->flush();
    }
}

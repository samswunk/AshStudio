<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class CreationRecherche
{

    /**
     * @var int
     * @Assert\Range(min=10,max=900)
     */
    private $prixMax;
    /**
     * @var int
     * @Assert\Range(min=1, max=50)
     */
    private $prixMin;

    /**
     * @var ArrayCollection
     */
    private $typeMatieres;

    /*
     * @var string
     */
    private $nomCreation;

    public function __construct(){
        $this->typeMatieres = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getPrixMax()
    {
        return $this->prixMax;
    }

    /**
     * @param mixed $prixMax
     */
    public function setPrixMax(int $prixMax)
    {
        $this->prixMax = $prixMax;
    }

    /**
     * @return mixed
     */
    public function getPrixMin()
    {
        return $this->prixMin;
    }

    /**
     * @param mixed $prixMin
     */
    public function setPrixMin(int $prixMin)
    {
        $this->prixMin = $prixMin;
    }

    /**
     * @return mixed
     */
    public function getNomCreation()
    {
        return $this->nomCreation;
    }

    /**
     * @param mixed $nomCreation
     */
    public function setNomCreation($nomCreation)
    {
        $this->nomCreation = $nomCreation;
    }

    /**
     * @return ArrayCollection
     */
    public function getTypeMatieres(): ArrayCollection
    {
        return $this->typeMatieres;
    }

    /**
     * @param ArrayCollection $typeMatieres
     */
    public function setTypeMatieres(ArrayCollection $typeMatieres):void
    {
        $this->typeMatieres = $typeMatieres;
    }


}
?>
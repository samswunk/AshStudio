<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nomUser;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $prenomUser;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ddnUser;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $emailUser;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Roles", inversedBy="UserRole")
     */
    private $TypeRole;

    public function getId(): int
    {
        return $this->id;
    }

    public function isGranted($role='ROLE_USER')
    {
        return (is_null($role)?'NO':in_array($role,$this->getTypeRole()));
    }

    /**
     * @return (Role|string)[] The user roles
     */

    public function getRoles()
    {
        return ['ROLE_ADMIN']; // (is_null($this->TypeRole)?['ROLE_USER']:$this->TypeRole);
    }

    /**
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password
        ]);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        return list($this->id,
            $this->username,
            $this->password) = unserialize($serialized, ['allowed_class'=>false]);
    }

    /**
     * Removes sensitive data from the user.
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    public function getNomUser(): ?string
    {
        return $this->nomUser;
    }

    public function setNomUser(?string $nomUser): self
    {
        $this->nomUser = $nomUser;

        return $this;
    }

    public function getPrenomUser(): ?string
    {
        return $this->prenomUser;
    }

    public function setPrenomUser(?string $prenomUser): self
    {
        $this->prenomUser = $prenomUser;

        return $this;
    }

    public function getDdnUser(): ?\DateTimeInterface
    {
        return $this->ddnUser;
    }

    public function setDdnUser(?\DateTimeInterface $ddnUser): self
    {
        $this->ddnUser = $ddnUser;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmailUser(): ?string
    {
        return $this->emailUser;
    }

    public function setEmailUser(string $emailUser): self
    {
        $this->emailUser = $emailUser;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getTypeRole(): ?Roles
    {
        return $this->TypeRole;
    }

    public function setTypeRole(?Roles $TypeRole): self
    {
        $this->TypeRole = $TypeRole;

        return $this;
    }
}

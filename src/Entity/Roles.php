<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RolesRepository")
 */
class Roles
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nomRole;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="TypeRole")
     */
    private $UserRole;

    public function __construct()
    {
        $this->UserRole = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomRole(): ?string
    {
        return $this->nomRole;
    }

    public function setNomRole(string $nomRole): self
    {
        $this->nomRole = $nomRole;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserRole(): Collection
    {
        return $this->UserRole;
    }

    public function addUserRole(User $userRole): self
    {
        if (!$this->UserRole->contains($userRole)) {
            $this->UserRole[] = $userRole;
            $userRole->setTypeRole($this);
        }

        return $this;
    }

    public function removeUserRole(User $userRole): self
    {
        if ($this->UserRole->contains($userRole)) {
            $this->UserRole->removeElement($userRole);
            // set the owning side to null (unless already changed)
            if ($userRole->getTypeRole() === $this) {
                $userRole->setTypeRole(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatiereRepository")
 */
class Matiere
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $nomMatiere;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionMatiere;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Creation", mappedBy="matiere")
     */
    private $matiereCreation;

    public function __construct()
    {
        $this->matiereCreation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomMatiere(): ?string
    {
        return $this->nomMatiere;
    }

    public function setNomMatiere(string $nomMatiere): self
    {
        $this->nomMatiere = $nomMatiere;

        return $this;
    }

    public function getDescriptionMatiere(): ?string
    {
        return $this->descriptionMatiere;
    }

    public function setDescriptionMatiere(?string $descriptionMatiere): self
    {
        $this->descriptionMatiere = $descriptionMatiere;

        return $this;
    }

    /**
     * @return Collection|Creation[]
     */
    public function getMatiereCreation(): Collection
    {
        return $this->matiereCreation;
    }

    public function addMatiereCreation(Creation $matiereCreation): self
    {
        if (!$this->matiereCreation->contains($matiereCreation)) {
            $this->matiereCreation[] = $matiereCreation;
            $matiereCreation->setMatiere($this);
        }

        return $this;
    }

    public function removeMatiereCreation(Creation $matiereCreation): self
    {
        if ($this->matiereCreation->contains($matiereCreation)) {
            $this->matiereCreation->removeElement($matiereCreation);
            // set the owning side to null (unless already changed)
            if ($matiereCreation->getMatiere() === $this) {
                $matiereCreation->setMatiere(null);
            }
        }

        return $this;
    }
}

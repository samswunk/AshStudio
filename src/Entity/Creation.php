<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CreationRepository")
 * @UniqueEntity("nomCreation")
 */
class Creation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Length(min=2,max=80,
     *      minMessage = "Au moins {{ limit }} caractères",
     *      maxMessage = "Max {{ limit }} caractères"
     * )
     * @ORM\Column(type="string", length=80)
     */
    private $nomCreation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * * @Assert\Length(min=10, max=255,
     *      minMessage = "Au moins {{ limit }} caractères",
     *      maxMessage = "Max {{ limit }} caractères"
     * )
     */
    private $descriptionCreation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min = 1, minMessage = "Minimum {{ limit }} €")
     */
    private $prixCreation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Url()
     */
    private $imageCreation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateCreation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matiere", inversedBy="matiereCreation")
     */
    private $typeMatiere;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCreation(): ?string
    {
        return $this->nomCreation;
    }

    public function setNomCreation(string $nomCreation): self
    {
        $this->nomCreation = $nomCreation;

        return $this;
    }

    public function getDescriptionCreation(): ?string
    {
        return $this->descriptionCreation;
    }

    public function setDescriptionCreation(?string $descriptionCreation): self
    {
        $this->descriptionCreation = $descriptionCreation;

        return $this;
    }

    public function getPrixCreation(): ?int
    {
        return $this->prixCreation;
    }

    public function setPrixCreation(?int $prixCreation): self
    {
        $this->prixCreation = $prixCreation;

        return $this;
    }

    public function getImageCreation(): ?string
    {
        return $this->imageCreation;
    }

    public function setImageCreation(?string $imageCreation): self
    {
        $this->imageCreation = $imageCreation;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getSlug(): string
    {
        return (new Slugify())->slugify($this->titre);
    }

    public function getTypeMatiere(): ?Matiere
    {
        return $this->typeMatiere;
    }

    public function setTypeMatiere(?Matiere $matiere): self
    {
        $this->typeMatiere = $matiere;

        return $this;
    }
}
